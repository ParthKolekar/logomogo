﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace LOGOMOGO
{
    public partial class MainPage : PhoneApplicationPage
    {
        const double sahay = Math.PI / 180;
        class Turtle
        {
            Point coords;
            int angle;
            Boolean pen;
            int thickness;
            Color color;

            public Turtle()
            {
                this.coords.X = 198;
                this.coords.Y = 233;

                this.angle = 90;

                this.pen = true;

                this.thickness = 1;

                this.color.R = 0;
                this.color.G = 0;
                this.color.B = 0;
                this.color.A = 255;
            }

            public Turtle(Point point, int angle, int thickness, Color color)
            {
                this.coords.X = point.X;
                this.coords.Y = point.Y;

                this.angle = angle;

                this.pen = true;

                this.thickness = thickness;

                this.color.R = color.R;
                this.color.G = color.G;
                this.color.B = color.B;
                this.color.A = color.A;
            }

            public void setCoords(Point coords)
            {
                this.coords.X = coords.X;
                this.coords.Y = coords.Y;
            }

            public void setAngle(int angle)
            {
                this.angle = angle;
            }

            public void setPen(Boolean pen)
            {
                this.pen = pen;
            }

            public void setThickness(int thickness)
            {
                this.thickness = thickness;
            }

            public void setColor(Color color)
            {
                this.color.R = color.R;
                this.color.G = color.G;
                this.color.B = color.B;
                this.color.A = color.A;
            }

            public Point getCoords()
            {
                return this.coords;
            }

            public int getAngle()
            {
                return this.angle;
            }

            public Boolean getPen()
            {
                return this.pen;
            }

            public int getThickness()
            {
                return this.thickness;
            }

            public Color getColor()
            {
                return this.color;
            }

            public void PU()
            {
                this.pen = false;
            }

            public void PD()
            {
                this.pen = true;
            }

            public void LT(int angle)
            {
                this.angle -= angle;
                this.angle = this.angle % 360;
            }

            public void RT(int angle)
            {
                this.angle += angle;
                this.angle = this.angle % 360;
            }

            public void FD(int distance)
            {
                this.coords.X -= distance * Math.Cos(this.angle * sahay);
                if (this.coords.X >= 396)
                    this.coords.X = 396;
                if (this.coords.X <= 0)
                    this.coords.X = 0;
                this.coords.Y -= distance * Math.Sin(this.angle * sahay);
                if (this.coords.Y >= 466)
                    this.coords.Y = 466;
                if (this.coords.Y <= 0)
                    this.coords.Y = 0;
            }

            public void BK(int distance)
            {
                this.coords.X += distance * Math.Cos(this.angle * sahay);
                if (this.coords.X >= 396)
                    this.coords.X = 396;
                if (this.coords.X <= 0)
                    this.coords.X = 0;
                this.coords.Y += distance * Math.Sin(this.angle * sahay);
                if (this.coords.Y >= 466)
                    this.coords.Y = 466;
                if (this.coords.Y <= 0)
                    this.coords.Y = 0;
            }

            public void SETPOS(Point point)
            {
                this.coords.X = point.X;
                if (this.coords.X >= 396)
                    this.coords.X = 396;
                if (this.coords.X <= 0)
                    this.coords.X = 0;
                this.coords.Y = point.Y;
                if (this.coords.Y >= 466)
                    this.coords.Y = 466;
                if (this.coords.Y <= 0)
                    this.coords.Y = 0;
            }

            public void SETCOL(Color color)
            {
                this.color.R = color.R;
                this.color.G = color.G;
                this.color.B = color.B;
                this.color.A = color.A;
            }

            public void HM()
            {
                this.coords.X = 198;
                this.coords.Y = 233;
                this.angle = 90;
            }

            public void SETPEN(int thickness)
            {
                this.thickness = thickness % 10;
            }

            public void SETX(int X)
            {
                this.coords.X += X;
                if (this.coords.X >= 396)
                    this.coords.X = 396;
                if (this.coords.X <= 0)
                    this.coords.X = 0;
            }

            public void SETY(int Y)
            {
                this.coords.Y += Y;
                if (this.coords.Y >= 466)
                    this.coords.Y = 466;
                if (this.coords.Y <= 0)
                    this.coords.Y = 0;
            }
        }

        Turtle turtle = new Turtle();

        public MainPage()
        {
            InitializeComponent();

            DataContext = App.ViewModel;
            this.Loaded += new RoutedEventHandler(MainPage_Loaded);

            this.CodePanelTextBox.Text = "cs setcol 100 100 224 setpen 1 hm rpt 1000 [ fd 150 bk 150 setcol 214 13 213 lt 3 fd 150 bk 150 setcol 41 15 35 lt 8 fd 150 bk 150 setcol 41 43 123 lt 9 fd 150 bk 150 setcol 1 134 211 lt 56 fd 150 bk 150 ]";

            FDE("cs setcol 100 100 224 setpen 1 hm rpt 1000 [ fd 150 bk 150 setcol 214 13 213 lt 3 fd 150 bk 150 setcol 41 15 35 lt 8 fd 150 bk 150 setcol 41 43 123 lt 9 fd 150 bk 150 setcol 1 134 211 lt 56 fd 150 bk 150 ] ".Split());
        }

        public void LineDraw(Point a, Point b, int thickness, Color color)
        {
            Line line = new Line();
            line.Stroke = new SolidColorBrush(color);
            line.StrokeThickness = thickness;

            line.X1 = a.X;
            line.Y1 = a.Y;
            line.X2 = b.X;
            line.Y2 = b.Y;

            this.CanvasPanelCanvas.Children.Add(line);
        }

        private void CS()
        {
            this.CanvasPanelCanvas.Children.Clear();
            turtle.HM();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.CanvasPanelCanvas.Children.Clear();
            this.CodePanelTextBox.Text = "";
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
                App.ViewModel.LoadData();
        }

        private bool isINT(string st)
        {
            int temp;
            return int.TryParse(st, out temp);
        }

        private void Go_Clicked(object sender, RoutedEventArgs e)
        {
            String st = this.CodePanelTextBox.Text;
            string[] words = st.Split();

            FDE(words);


        }

        private void FDE(string[] words)
        {
            Color color = new Color();
            Point point = new Point();

            int result = words.Count(s => s != null);

            int i = 0;
            string word = words[0];                     //FETCH

            if (isINT(word))                            //DECODE
            {
                //errerr
            }
            else
            {
                for (i = 0; i < result; i++)
                {
                    word = words[i];

                    if (word == "")
                        continue;

                    switch (word[0])                     //EXCUTE
                    {
                        case 'f':
                            i = i + 1;
                            word = words[i];
                            if (!isINT(word))
                            {
                                //errerr
                            }
                            else
                            {
                                int temp;
                                int.TryParse(word, out temp);
                                var prev = turtle.getCoords();
                                turtle.FD(temp);
                                if (turtle.getPen())
                                    LineDraw(prev, turtle.getCoords(), turtle.getThickness(), turtle.getColor());
                                //LineDraw1();
                            }
                            break;
                        case 'b':
                            i = i + 1;
                            word = words[i];
                            if (!isINT(word))
                            {
                                //errerr
                            }
                            else
                            {
                                int temp;
                                int.TryParse(word, out temp);
                                var prev = turtle.getCoords();
                                turtle.BK(temp);
                                if (turtle.getPen())
                                    LineDraw(prev, turtle.getCoords(), turtle.getThickness(), turtle.getColor());
                                //LineDraw1();

                            }
                            break;
                        case 'l':
                            i = i + 1;
                            word = words[i];
                            if (!isINT(word))
                            {
                                //errerr
                            }
                            else
                            {
                                int temp;
                                int.TryParse(word, out temp);
                                turtle.LT(temp);
                                //LineDraw1();
                            }
                            break;
                        case 'r':
                            switch (word[1])
                            {
                                case 't':
                                    i = i + 1;
                                    word = words[i];
                                    if (!isINT(word))
                                    {
                                        //errerr
                                    }
                                    else
                                    {
                                        int temp;
                                        int.TryParse(word, out temp);
                                        turtle.RT(temp);
                                        //LineDraw1();
                                    }
                                    break;
                                case 'p':
                                    i = i + 1;
                                    word = words[i];
                                    if (!isINT(word))
                                    {
                                        //errerr
                                    }
                                    else
                                    {
                                        int temp;
                                        int.TryParse(word, out temp);
                                        i = i + 1;
                                        word = words[i];
                                        if (word != "[")
                                        {
                                            //errerr
                                        }
                                        else
                                        {
                                            string[] tempWords = new string[words.Length + 1];
                                            i = i + 1;
                                            word = words[i];
                                            var l = 0;
                                            while (word != "]")
                                            {
                                                tempWords[l] = word;
                                                l = l + 1;
                                                i = i + 1;
                                                word = words[i];
                                            }
                                            for (var k = 0; k < temp; k++)
                                                FDE(tempWords);
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 'p':
                            switch (word[1])
                            {
                                case 'u':
                                    turtle.PU();
                                    //LineDraw1();
                                    break;
                                case 'd':
                                    turtle.PD();
                                    //LineDraw1();
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 's':
                            switch (word[3])
                            {
                                case 'p':
                                    switch (word[4])
                                    {
                                        case 'o':
                                            i = i + 1;
                                            word = words[i];
                                            if (!isINT(word))
                                            {
                                                //errerr
                                            }
                                            else
                                            {
                                                int temp1;
                                                int.TryParse(word, out temp1);
                                                i = i + 1;
                                                word = words[i];
                                                if (!isINT(word))
                                                {
                                                    //errerr
                                                }
                                                else
                                                {
                                                    int temp2;
                                                    int.TryParse(word, out temp2);
                                                    point.X = temp1;
                                                    point.Y = temp2;
                                                    turtle.SETPOS(point);
                                                    //LineDraw1();
                                                }
                                            }
                                            break;
                                        case 'e':
                                            i = i + 1;
                                            word = words[i];
                                            if (!isINT(word))
                                            {
                                                //errerr
                                            }
                                            else
                                            {
                                                int temp;
                                                int.TryParse(word, out temp);
                                                turtle.SETPEN(temp);
                                                //LineDraw1();
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                                case 'c':
                                    i = i + 1;
                                    word = words[i];
                                    if (!isINT(word))
                                    {
                                        //errerr
                                    }
                                    else
                                    {
                                        int temp1;
                                        int.TryParse(word, out temp1);
                                        i = i + 1;
                                        word = words[i];
                                        if (!isINT(word))
                                        {
                                            //errerr
                                        }
                                        else
                                        {
                                            int temp2;
                                            int.TryParse(word, out temp2);
                                            i = i + 1;
                                            word = words[i];
                                            if (!isINT(word))
                                            {
                                                //errerr
                                            }
                                            else
                                            {
                                                int temp3;
                                                int.TryParse(word, out temp3);
                                                color.R = (byte)temp1;
                                                color.G = (byte)temp2;
                                                color.B = (byte)temp3;
                                                color.A = 255;
                                                turtle.SETCOL(color);
                                                //LineDraw1();
                                            }
                                        }
                                    }
                                    break;
                                case 'x':
                                    i = i + 1;
                                    word = words[i];
                                    if (!isINT(word))
                                    {
                                        //errerr
                                    }
                                    else
                                    {
                                        int temp;
                                        int.TryParse(word, out temp);
                                        turtle.SETX(temp);
                                        //LineDraw1();
                                    }
                                    break;
                                case 'y':
                                    i = i + 1;
                                    word = words[i];
                                    if (!isINT(word))
                                    {
                                        //errerr
                                    }
                                    else
                                    {
                                        int temp;
                                        int.TryParse(word, out temp);
                                        turtle.SETY(temp);
                                        //LineDraw1();
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 'c':
                            CS();
                            //LineDraw1();
                            break;
                        case 'h':
                            turtle.HM();
                            //LineDraw1();
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void sampleShapes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CS();

            switch (sampleShapes.SelectedIndex)
            {
                case 0:
                    this.CodePanelTextBox.Text = "setcol 0 0 255 rpt 3 [ fd 100 rt 120 ] ";
                    break;
                case 1:
                    this.CodePanelTextBox.Text = "setcol 0 255 0 rpt 4 [ fd 100 rt 90 ] ";
                    break;
                case 2:
                    this.CodePanelTextBox.Text = "setcol 0 255 255 rpt 5 [ fd 100 rt 72 ] ";
                    break;
                case 3:
                    this.CodePanelTextBox.Text = "setcol 255 0 0 rpt 6 [ fd 100 rt 60 ] ";
                    break;
                case 4:
                    this.CodePanelTextBox.Text = "setcol 255 0 255 rpt 360 [ fd 1 rt 1 ]";
                    break;
                case 5:
                    this.CodePanelTextBox.Text = "setcol 0 0 255 lt 90 fd 60 rt 90 fd 120 hm setcol 255 0 0 pu rt 90 fd 10 lt 90 pd fd 120 lt 225 fd 40 lt 90 fd 40 rt 135 fd 120";
                    break;

                default:
                    break;
            }
            FDE(this.CodePanelTextBox.Text.Split());
        }
        private void sampleCommands_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sampleCommands.SelectedIndex == 0)
            {
                MessageBox.Show("fd \n" + "Moves Turtle forward by x\n" + "Usage : fd x");
            }
            if (sampleCommands.SelectedIndex == 1)
            {
                MessageBox.Show("bk \n" + "Moves Turtle backward by x\n" + "Usage : bk x");
            }
            if (sampleCommands.SelectedIndex == 2)
            {
                MessageBox.Show("rt \n" + "Rotate Turtle right by x\n" + "Usage : rt x");
            }
            if (sampleCommands.SelectedIndex == 3)
            {
                MessageBox.Show("lt \n" + "Rotate Turtle left by x\n" + "Usage : lt x");
            }
            if (sampleCommands.SelectedIndex == 4)
            {
                MessageBox.Show("pu \n" + "Moves Pen up, the Turtle won't draw\n" + "Usage : pu");
            }
            if (sampleCommands.SelectedIndex == 5)
            {
                MessageBox.Show("pd \n" + "Moves pen down, the Turtle will draw\n" + "Usage : pd");
            }
            if (sampleCommands.SelectedIndex == 6)
            {
                MessageBox.Show("hm \n" + "Moves Turtle to default\n" + "Usage : hm");
            }
            if (sampleCommands.SelectedIndex == 7)
            {
                MessageBox.Show("cs \n" + "Clears the Screen and moves Turtle to default\n" + "Usage : cs");
            }
            if (sampleCommands.SelectedIndex == 8)
            {
                MessageBox.Show("rpt \n" + "Repeats given Command x times\n" + "Usage : rpt x [ 'your command here' ]");
            }
            if (sampleCommands.SelectedIndex == 9)
            {
                MessageBox.Show("setcol \n" + "Sets Color of pen to given r,g,b values\n" + "Usage : setcol r g b");
            }
            if (sampleCommands.SelectedIndex == 10)
            {
                MessageBox.Show("setpos \n" + "Sets Turtle Position Co-ordinates\n" + "Usage : setpos x y");
            }
            if (sampleCommands.SelectedIndex == 11)
            {
                MessageBox.Show("setpen \n" + "Sets Pen Size to x\n" + "Usage : setpen x");
            }
            if (sampleCommands.SelectedIndex == 12)
            {
                MessageBox.Show("setx \n" + "Sets Turtle Position X Co-ordinate relative to current Co-ordinate\n" + "Usage : setx x ");
            }
            if (sampleCommands.SelectedIndex == 13)
            {
                MessageBox.Show("sety \n" + "Sets Turtle Position Y Co-ordinate relative to current Co-ordinate\n" + "Usage : sety y");
            }
        }
    }
}
